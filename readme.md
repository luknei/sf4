## Installation

```
php composer.phar install
```

## Setup

```
cp .env.dist .env
``` 
```
php bin/console app:import:messages src/Seed/messages_sample.json
``` 

## Testing

before running tests
```
cp phpunit.xml.dist phpunit.xml
``` 
 
configure DB connection env variables in ```phpunit.xml```

```
...
<env name="DATABASE_URL" value="{mysql}://{usrname}:psw@127.0.0.1:3306/{db}" />
...
```

## Security

Simple api token based security is implemented

all routes require to send X-AUTH-TOKEN with every request
currently hardcoded api key: ``` real-one ```

```
    X-AUTH-TOKEN: real-one
```

## Data structures

```
Message {
    uid: string,
    sender: sring,
    subject: string,
    message: string,
    time_sent: string,
}

PaginationData {
    page: int,
    totalPages: int,
}
```

## API

### Messages

``` GET /api/v1/messages ```

##### Parameters 

Name | Type | Mandatory | Description
------------ | ------------ | ------------ | ------------
page | integer | NO | page no of messages

##### Response 

```
{
    data: Message[],
    meta: PaginationData,
}
```

### Archived messages

``` GET /api/v1/archive/messages ```

##### Parameters 

Name | Type | Mandatory | Description
------------ | ------------ | ------------ | ------------
page | integer | NO | page no of messages

##### Response 

```
{
    data: Message[],
    meta: PaginationData,
}
```

### Single message

``` GET /api/v1/messages/{messageId} ```

##### Parameters 

Name | Type | Mandatory | Description
------------ | ------------ | ------------ | ------------
messageId | string | YES | message ID

##### Response 

```
{
    data: Message,
}
```


### Mark message as read

``` POST /api/v1/messages/{messageId}/read ```

##### Parameters 

Name | Type | Mandatory | Description
------------ | ------------ | ------------ | ------------
messageId | string | YES | message ID

##### Response 

```
{
    data: {
        message: string
    },
}
```

### Mark message as read

``` POST /api/v1/messages/{messageId}/archive ```

##### Parameters 

Name | Type | Mandatory | Description
------------ | ------------ | ------------ | ------------
messageId | string | YES | message ID

##### Response 

```
{
    data: {
        message: string
    },
}
```

## TODO

* Truncate tables before functional tests
* Wrap integration tests into DB transactions
* Write more tests
* Setup static code analyzers phpcs, phpmd, phpstan etc
* Move archived messages to separate table
* Add API error handler & error data structure
* Add Data structure for DateTime transformation