<?php

namespace App\Tests\functional\Controller;

class MessagesControllerTest extends ControllerTestCase
{
    public function testCorrectAuthToken()
    {
        $client = static::createClient();

        $client->request('GET', '/api/v1/messages', [], [], [
            'HTTP_X-AUTH-TOKEN' => 'real-one',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(200, $response->getStatusCode(), substr($response->getContent(), 0, 500));
    }

    public function testWrongAuthToken()
    {
        $client = static::createClient();

        $client->request('GET', '/api/v1/messages', [], [], [
            'HTTP_X-AUTH-TOKEN' => 'wrong-one',
        ]);

        $response = $client->getResponse();

        $this->assertEquals(403, $response->getStatusCode(), substr($response->getContent(), 0, 500));
    }

    public function testNotToken()
    {
        $client = static::createClient();

        $client->request('GET', '/api/v1/messages');

        $response = $client->getResponse();

        $this->assertEquals(401, $response->getStatusCode(), substr($response->getContent(), 0, 500));
    }
}