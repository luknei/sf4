<?php

namespace App\Tests\unit\Transformer;

use App\Entity\Message;
use App\Transformer\MessageTransformer;
use PHPUnit\Framework\MockObject\MockObject;

class MessageTransformerTest extends TransformerTestCase
{
    public function testTransform()
    {
        $ts = new \DateTime();

        $transformer = new MessageTransformer();
        /** @var Message|MockObject $message */
        $message = $this->createCustomMock(Message::class, [
            'getId' => 'abc123',
            'getSender' => 'test sender',
            'getSubject' => 'test subject',
            'getMessage' => 'test message',
            'getSentAt' => $ts,
        ]);

        $expected = [
            'uid' => 'abc123',
            'sender' => 'test sender',
            'subject' => 'test subject',
            'message' => 'test message',
            'time_sent' => date_format($ts, \DateTime::ISO8601),
        ];
        $actual = $transformer->transform($message);

        static::assertEquals($expected, $actual);
    }
}