<?php

namespace App\Tests\unit\Transformer;

use App\Tests\unit\UnitTestCase;

abstract class TransformerTestCase extends UnitTestCase
{
    protected function createCustomMock(string $originalClassName, array $returnByMethod)
    {
        $mock = parent::createMock($originalClassName);

        foreach ($returnByMethod as $method => $returnValue) {
            $mock->expects(static::once())
                ->method($method)
                ->willReturn($returnValue);
        }

        return $mock;
    }
}