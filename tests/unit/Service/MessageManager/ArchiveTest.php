<?php

namespace App\Tests\unit\Service\MessageManager;

use App\Entity\Message;

class ArchiveTest extends TestCase
{
    /**
     * @throws \Exception
     */
    public function testChangesArchivedProperty()
    {
        $message = new Message('john', 'test', 'no msg');

        static::assertFalse($message->isArchived());

        $this->entityManager->expects(static::once())
            ->method('persist')
            ->with($message);

        $this->entityManager->expects(static::once())
            ->method('flush');

        $this->instance->archiveMessage($message);

        static::assertTrue($message->isArchived());
    }
}