<?php

namespace App\Tests\unit\Service\MessageManager;

use App\Repository\MessageRepository;
use App\Service\MessageManager;
use App\Tests\unit\Service\ServiceTestCase;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;

abstract class TestCase extends ServiceTestCase
{
    /** @var EntityManagerInterface|MockObject */
    protected $entityManager;

    /** @var MessageRepository|MockObject */
    protected $messageRepo;

    /** @var MessageManager */
    protected $instance;

    public function setUp()
    {
        $this->entityManager = $this->createMock(EntityManagerInterface::class);
        $this->messageRepo = $this->createMock(MessageRepository::class);

        $this->instance = new MessageManager($this->entityManager, $this->messageRepo);
    }
}
