<?php

namespace App\Tests\integration\Repository\MessageRepository;

use App\Repository\MessageRepository;
use App\Tests\integration\Repository\RepositoryTestCase;

class TestCase extends RepositoryTestCase
{
    /** @var MessageRepository */
    protected $instance;

    public function setUp()
    {
        parent::setUp();

        $this->instance = static::$container->get(MessageRepository::class);
    }
}