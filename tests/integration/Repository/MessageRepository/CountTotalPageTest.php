<?php

namespace App\Tests\integration\Repository\MessageRepository;


use App\Entity\Message;

class CountTotalPageTest extends TestCase
{
    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function testNoMessages()
    {
        $messagesCount = $this->instance->countTotalPage(false);

        static::assertEquals(1, $messagesCount);
    }

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Exception
     */
    public function testTwoPagesOfMessages()
    {
        $this->createMessages(6);

        $messagesCount = $this->instance->countTotalPage(false);

        static::assertEquals(2, $messagesCount);
    }

    /**
     * @param int $count
     * @throws \Exception
     */
    protected function createMessages($count = 1)
    {
        for ($i = 0; $i < $count; $i++) {
            $message = new Message($this->faker->name, $this->faker->sentence, $this->faker->text);

            $this->entityManager->persist($message);
        }

        $this->entityManager->flush();
    }
}