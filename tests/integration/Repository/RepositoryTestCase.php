<?php

namespace App\Tests\integration\Repository;

use Composer\Console\Application;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\ORM\EntityManagerInterface;
use Faker\Factory;
use Faker\Generator;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class RepositoryTestCase extends KernelTestCase
{
    /** @var string[] */
    protected $fixtures = [];

    /** @var EntityManagerInterface */
    protected $entityManager;

    /** @var Generator */
    protected $faker;

    /**
     * @throws \Exception
     */
    public function setUp()
    {
        parent::setUp();

        $this->bootKernel();
        $this->entityManager = static::$container->get(EntityManagerInterface::class);
        $this->faker = Factory::create();

        $fixtures = $this->getFixtures();

        // Purge tables
        $this->purgeDb();

        if (!empty($fixtures)) {
            // Load fixtures
            $this->fillDb();
        }
    }

    /**
     * Get all fixtures
     *
     * @return array
     */
    protected function getFixtures()
    {
        return array_unique($this->fixtures);
    }

    /**
     * Just purge database
     *
     * @throws \Exception
     */
    public function purgeDb()
    {
        $container = static::$container;
        $entityManager = $container->get('doctrine')->getManager();

        // Purge tables
        $purger = new ORMPurger($entityManager);
        $purger->setPurgeMode(ORMPurger::PURGE_MODE_TRUNCATE);
        $executor = new ORMExecutor($entityManager, $purger);
        $executor->purge();
    }

    /**
     * Fill database with fixtures data
     */
    public function fillDb()
    {
        $fixtures = $this->getFixtures();

        if (empty($fixtures)) {
            return;
        }

        $args = array_map(function (string $fixtureClass) {
            return '--fixtures=' . escapeshellarg($fixtureClass);
        }, $fixtures);

        $command = sprintf('doctrine:fixtures:load %s', join(' ', $args));

        $this->runCommand($command);
    }

    protected static function runCommand($command)
    {
        $command = sprintf('%s --quiet --append --env=test', $command);

        return self::getApplication()->run(new StringInput($command));
    }

    protected static function getApplication()
    {
        if (null === self::$application) {
            self::$application = new Application();
            self::$application->setAutoExit(false);
        }

        return self::$application;
    }
}