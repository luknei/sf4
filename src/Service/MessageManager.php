<?php

namespace App\Service;

use App\Repository\MessageRepository;
use Carbon\Carbon;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Message;

class MessageManager
{
    /** @var EntityManagerInterface */
    protected $entityManager;

    /** @var MessageRepository */
    protected $messageRepo;

    /**
     * @param EntityManagerInterface $entityManager
     * @param MessageRepository $messageRepo
     */
    public function __construct(EntityManagerInterface $entityManager, MessageRepository $messageRepo)
    {
        $this->entityManager = $entityManager;
        $this->messageRepo = $messageRepo;
    }

    /**
     * @param Message $message
     */
    public function markAsRead(Message $message)
    {
        $message->markAsRead();

        $this->entityManager->persist($message);
        $this->entityManager->flush();
    }

    /**
     * @param Message $message
     */
    public function archiveMessage(Message $message)
    {
        $message->archive();

        $this->entityManager->persist($message);
        $this->entityManager->flush();
    }

    /**
     * @param array $messages
     * @throws \Exception
     */
    public function import(array $messages)
    {
        foreach ($messages as $data) {
            $message = new Message($data['sender'], $data['subject'], $data['message']);
            $message->setId($data['uid']);
            $message->setSentAt(Carbon::createFromTimestamp($data['time_sent']));

            $this->entityManager->persist($message);
        }

        $this->entityManager->flush();
    }
}