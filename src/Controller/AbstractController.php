<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;

abstract class AbstractController
{
    protected function jsonResponse(array $data, array $meta = null)
    {
        $payload = [
            'data' => $data,
        ];

        if ($meta) {
            $payload['meta'] = $meta;
        }

        return new JsonResponse($payload);
    }
}