<?php

namespace App\Controller;

use App\Entity\Message;
use App\Repository\MessageRepository;
use App\Service\MessageManager;
use App\Transformer\MessageTransformer;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class MessagesController extends AbstractController
{
    /** @var MessageManager */
    protected $messageManager;

    /** @var MessageRepository */
    protected $messageRepo;

    /** @var MessageTransformer */
    protected $messageTransformer;

    /**
     * @param MessageManager $messageManager
     * @param MessageRepository $messageRepo
     * @param MessageTransformer $messageTransformer
     */
    public function __construct(
        MessageManager $messageManager,
        MessageRepository $messageRepo,
        MessageTransformer $messageTransformer
    ) {
        $this->messageManager = $messageManager;
        $this->messageRepo = $messageRepo;
        $this->messageTransformer = $messageTransformer;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function listAction(Request $request)
    {
        $page = $request->query->get('page', 1);

        $messages = $this->messageRepo->getMessages(false, $page);
        $totalPages = $this->messageRepo->countTotalPage(false);

        $data = $this->messageTransformer->transformArray($messages);
        $meta = [
            'page' => $page,
            'totalPages' => $totalPages,
        ];

        return $this->jsonResponse($data, $meta);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function listArchiveAction(Request $request)
    {
        $page = $request->query->get('page', 1);

        $messages = $this->messageRepo->getMessages(true, $page);
        $totalPages = $this->messageRepo->countTotalPage(true);

        $data = $this->messageTransformer->transformArray($messages);
        $meta = [
            'page' => $page,
            'totalPages' => $totalPages,
        ];

        return $this->jsonResponse($data, $meta);
    }

    /**
     * @param Message $message
     * @return JsonResponse
     */
    public function messageAction(Message $message)
    {
        $data = $this->messageTransformer->transform($message);

        return $this->jsonResponse($data);
    }

    /**
     * @param Message $message
     */
    public function markAsReadAction(Message $message)
    {
        $this->messageManager->markAsRead($message);

        $this->jsonResponse([
            'message' => sprintf('Message "%s" was marked as read', $message->getId()),
        ]);
    }

    /**
     * @param Message $message
     */
    public function archiveMessageAction(Message $message)
    {
        $this->messageManager->archiveMessage($message);

        $this->jsonResponse([
            'message' => sprintf('Message "%s" was archived', $message->getId()),
        ]);
    }
}