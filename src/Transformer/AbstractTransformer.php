<?php

namespace App\Transformer;

abstract class AbstractTransformer
{
    /**
     * @param $entity
     * @return array
     */
    abstract public function transform($entity);

    /**
     * @param array $array
     * @return array
     */
    public function transformArray(array $array): array
    {
        $transformerArray = array_map(function ($entity) {
            return $this->transform($entity);
        }, $array);

        return array_values($transformerArray);
    }
}