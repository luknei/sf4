<?php

namespace App\Transformer;

use App\Entity\Message;

class MessageTransformer extends AbstractTransformer
{
    /**
     * @param Message $entity
     * @return array
     */
    public function transform($entity)
    {
        return [
            'uid' => $entity->getId(),
            'sender' => $entity->getSender(),
            'subject' => $entity->getSubject(),
            'message' => $entity->getMessage(),
            'time_sent' => date_format($entity->getSentAt(), \DateTime::ISO8601),
        ];
    }

}
