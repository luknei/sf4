<?php

namespace App\Repository;

use Doctrine\ORM\NonUniqueResultException;
use App\Entity\Message;

class MessageRepository extends AbstractEntityRepository
{
    const PER_PAGE = 5;

    /**
     * @param bool $archived
     * @param int $page
     * @param int $perPage
     * @return Message[]
     */
    public function getMessages(bool $archived, int $page, int $perPage = self::PER_PAGE)
    {
        $offset = ($page - 1) * 20;
        $builder = $this->createQueryBuilder('message');

        $query = $builder
            ->where(
                $builder->expr()->eq('message.archived', ':archived')
            )
            ->orderBy('message.sentAt', 'DESC')
            ->setFirstResult($offset)
            ->setMaxResults($perPage)
            ->setParameter('archived', $archived);

        return $query->getQuery()->getResult();
    }

    /**
     * @param bool $archived
     * @param int $perPage
     * @return int
     * @throws NonUniqueResultException
     */
    public function countTotalPage(bool $archived, int $perPage = self::PER_PAGE)
    {
        $builder = $this->createQueryBuilder('message');

        $query = $builder
            ->select('COUNT(message.id) as cnt')
            ->where(
                $builder->expr()->eq('message.archived', ':archived')
            )
            ->setParameter('archived', $archived);

        $msgCount = $query->getQuery()->getSingleScalarResult();
        $totalPagesCount = ceil($msgCount / $perPage);

        return max($totalPagesCount, 1);
    }
}