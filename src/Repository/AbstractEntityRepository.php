<?php

namespace App\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

class AbstractEntityRepository extends EntityRepository
{
    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $entityClassName = $this->getEntityClassName();

        $meta = $entityManager->getClassMetadata($entityClassName);

        parent::__construct($entityManager, $meta);
    }

    /**
     * @return string
     */
    public function getEntityClassName(): string
    {
        $className = substr(static::class, 0, -10);

        return str_replace('Repository', 'Entity', $className);
    }

}