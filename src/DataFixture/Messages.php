<?php

namespace App\DataFixture;

use App\Entity\Message;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class Messages extends Fixture
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $message = new Message('test sender', 'test subject', 'test message');

        $manager->persist($message);

        $manager->flush();
    }
}