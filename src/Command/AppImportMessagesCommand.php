<?php

namespace App\Command;

use App\Service\MessageManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AppImportMessagesCommand extends Command
{
    protected static $defaultName = 'app:import:messages';

    /** @var MessageManager */
    protected $messageManager;

    /**
     * @param MessageManager $messageManager
     */
    public function __construct(MessageManager $messageManager)
    {
        parent::__construct();
        $this->messageManager = $messageManager;
    }

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('path', InputArgument::REQUIRED, 'Path to json messages')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $filePath = $input->getArgument('path');
        $contents = file_get_contents($filePath);
        $data = json_decode($contents, true);
        $messages = $data['messages'];

        $this->messageManager->import($messages);

        $output->writeln(sprintf('%d messages were imported.', count($messages)));
    }
}
